<?php
header('Content-type: text/html; charset=utf-8');
date_default_timezone_set('Asia/Ho_Chi_Minh');

echo "<div class='container' style='
padding: 12px 20px;
margin: 8px 0px;
border: 3px solid #ccc;
border-color: #008CBA;
// box-sizing: border-box;
'>";
echo "<div class='time' style='
color: black;
background-color: #F2F2F2;
width: 40.25%;
padding: 12px 20px;
margin: 8px 0px;
display: inline-block;
border: 3px solid #ccc;
border-color: #F2F2F2;
box-sizing: border-box;
font-size: 21;
font-family:'Times New Roman', Times, serif;
'>";
echo "Bây giờ là: ", date("H:i d/m/Y");
echo "</div>";
?>
<html>

<body>

    <form action="login.php" method="post">
        <style>
            .input[type=text],
            .input[type=password],
            select {
                width: 20%;
                padding: 12px 20px;
                margin: 8px 0;
                display: inline-block;
                border: 3px solid #ccc;
                border-color: #008CBA;
                box-sizing: border-box;
                font-size: 21;
                font-family: 'Times New Roman', Times, serif;
            }

            .label {
                color: white;
                background-color: #008CBA;
                width: 20%;
                padding: 12px 20px;
                margin: 8px 0;
                display: inline-block;
                border: 3px solid #ccc;
                border-color: #008CBA;
                box-sizing: border-box;
                font-size: 21;
                font-family: 'Times New Roman', Times, serif;
            }

            .button {
                color: white;
                background-color: #008CBA;
                width: 20%;
                padding: 12px 20px;
                margin: 8px 150px;
                display: inline-block;
                border: 3px solid #ccc;
                border-radius: 8px;
                border-color: #008CBA;
                box-sizing: border-box;
                cursor: pointer;
                font-size: 21;
                font-family: 'Times New Roman', Times, serif;
            }
        </style>


        <div class="form-group">
            <label for="name" class="label">Tên đăng nhập</label>
            <input type="text" class="input" name="name">
        </div>

        <div class="form-group">
            <label for="password" class="label">Mật khẩu</label>
            <input type="password" class="input" name="password">
        </div>

        <button type="submit" class="button" name="login">Đăng nhập</button>
        </div>
        </div>
        </div>
    </form>

</body>

</html>